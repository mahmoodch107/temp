using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator anim;

    [SerializeField] private AudioSource deathSoundEffect;
    [SerializeField] private Transform player;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
           var dirX = Input.GetAxisRaw("Horizontal");
            rb.velocity = new Vector2(dirX * 1f, rb.velocity.y);
            player.transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
            Die();
        }
    }

    private void Die()
    {
        deathSoundEffect.Play();
        //rb.bodyType = RigidbodyType2D.Static;
         anim.SetTrigger("death");
    }

    private void RestartLevel()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        
       // transform.position = new Vector3(player.position.x - 5f, player.position.y, transform.position.z);
    }
}
