using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TJumpUp : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float jumpForce = 21f;
    [SerializeField] private AudioSource jumpSoundEffect;
    private bool is_jumping = false;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        is_jumping = false;
}
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("TJumpUp"))
        {
             jumpSoundEffect.Play();
           // Debug.Log("T Jumping="+ collision.gameObject.transform.position.x);
            collision.gameObject.transform.position = new Vector3(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y + 1f, transform.position.z);
          
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.gameObject.transform.position = new Vector3(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y - 1f, transform.position.z);
    }
}
