using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawRotation : MonoBehaviour
{
    [SerializeField] float rotateSpeed = 60f;
    [SerializeField] int endPosition = 90;
    [SerializeField] int startPosition = 0;

    void Update()
    {
        transform.localEulerAngles = new Vector3(0, 0, Mathf.PingPong(Time.time * rotateSpeed, endPosition) + (startPosition));

    }
}
