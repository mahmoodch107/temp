using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /*[SerializeField] private Transform player;

    private void Update()
    {
        transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
    }*/

    [SerializeField] public Transform playerCharacter;
    public float cameraZPosition = -10f;
    [SerializeField] private float offset_y = 3f;
    [SerializeField] private float offset_y_updown = 2f;
    private void Start()
    {
        offset_y = 3f;
        offset_y_updown = 2f;
    }

    private void Update()
    {
        //Have the camera follow the player horizontally but not vertically
        Vector3 cameraPosition = transform.position;

        cameraPosition.x = playerCharacter.position.x;

        var cam_old_y_with_offset_up = cameraPosition.y + offset_y;
        if (playerCharacter.position.y > cam_old_y_with_offset_up)
        {
            cameraPosition.y = playerCharacter.position.y;
            cameraPosition.y = cameraPosition.y - offset_y_updown;
        }
        else
        {
            cameraPosition.y = cameraPosition.y - offset_y_updown;
        }
        var cam_old_y_with_offset_down = cameraPosition.y - offset_y;
        if (playerCharacter.position.y < cam_old_y_with_offset_down)
        {
            cameraPosition.y = playerCharacter.position.y;
            cameraPosition.y = cameraPosition.y - offset_y_updown;
        }
        else
        {
            cameraPosition.y = cameraPosition.y + offset_y_updown;
        }
        if (Input.GetKey("f"))
        {
            cameraPosition.y = playerCharacter.position.y + offset_y;
        }

        cameraPosition.z = cameraZPosition;

        transform.position = cameraPosition;


        //Have the camera look at the player's horizontal position without regard
        //to the player's vertical position
        Vector3 playerPosition = playerCharacter.position;

        playerPosition.x = playerPosition.x;
        playerPosition.y = transform.position.y;
        playerPosition.z = playerPosition.z;

        transform.LookAt(playerPosition);
    }
}
